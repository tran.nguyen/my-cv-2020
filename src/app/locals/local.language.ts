import {ILocalLanguage} from "../models/local.iterface";

export const LocalLanguage: ILocalLanguage = {
    en: {
        name_page: {
            hello: "Hi, I'm",
            title: "FRONTEND DEVELOPER",
            download: "Download Resume"
        },
        menu: {
            resume: "Resume",
            projects: "Projects",
            hobbies: "Hobbies",
            contact: "Contact",
        },
        resume_page: {
            title: "Profile",
            text_introduce: "I'm a <strong>Frontend Developer</strong> from Ho Chi Minh City, VN. I aim to make a difference through my creative solution.",
            text_subIntro: "Worked with project teams to create user-friendly and appealing application interfaces and websites for users. ",
            labels: {
                name: "Name",
                address: "Address",
                birth_day: "Date of birth",
                phone: "Phone",
                interest: "Interest ",
                skill: "SKILLS",
                experience: "EXPERIENCE",
                year_ex: "years experience"
            },
            experience_itro1: "During my time working at the company, I have had experience in working with frontend web development technologies such as Angular (6,7,8,9), Reacjs, Webpack, Module Builder (Ant Design) , Material UI etc.), as well as backend development technologies such as Nodejs, Socket io.",
            experience_itro2: "At the same time, I also have experience in project management as well as assigning work to team members to complete the company's projects.",
            experience_itro3: "At this stage, I studied the web development technologies such as Javascript, AngularJs, PHP, CodeIgniter MVC etc.",
            experience_itro4: "At university, I learned more in depth programming programming, data structures, software development processes, etc."
        },
        project_page: {
            title: "PROJECTS",
            label_filter: "Filter",
            label_all: "All"

        },
        hobbies: {
            title: "My Hobbies",
            reading: {
                title: "Learn thing form good book",
                sub_title: "I love reading. Reading can be one of the best habits I have. Thanks to daily reading, I was able to supplement my knowledge and learn a lot from books"
            },
            workout: {
                title: "WORKOUT",
                sub_title: "I love going to the gym because it enhances my health, helps me relax and helps me to regain energy after a hard working day."
            }
        },
        contact: {
            title: "Get In Touch",
            content: "  As one of the maxims in life that I like the most. <br/>  " +
                "I hope that my skills and experience can help your team develop and become more professional.",
            labels: {
                address: "Address",
                call: "Call Me",
            }
        }

    },
    vi: {
        name_page: {
            hello: "Xin chào, Tôi là",
            title: "Lập trình viên FRONTEND",
            download: "Download CV"
        },
        menu: {
            resume: "Tóm Tắt",
            projects: "Dự Án",
            hobbies: "Sở Thích",
            contact: "Liên Lạc",
        },
        resume_page: {
            title: "Profile",
            text_introduce: "Tôi là  <strong>lập trình viên Frontend</strong>  đến từ thành phố Hồ Chí Minh, VN. Tôi đặt mục tiêu tạo ra sự khác biệt thông qua giải pháp sáng tạo của mình.",
            text_subIntro: " Làm việc với các nhóm dự án để tạo ra các giao diện và trang web ứng dụng thân thiện và hấp dẫn cho người dùng.",
            labels: {
                name: "Tên",
                address: "Địa chỉ",
                birth_day: "Ngày sinh",
                phone: "Phone",
                interest: "Hứng thú",
                skill: "Kỹ năng",
                experience: "Kinh Nghiệm",
                year_ex: "Năm kinh nghiệm"
            },
            experience_itro1: "Trong quá trình công tác và làm việc tại công ty Alta Software, \n" +
                "Tôi đã có kinh nghiệm làm việc với các công nghệ phát triển web frontend như Angular (6,7,8,9), NGrx, Reacjs, Module Builder (Ant Design) , Material UI ), Webpack, Cũng như có kinh nghiệm làm việc với ngôn ngữ server: Nodejs, Socket io.",
            experience_itro2: "Đồng thời, Tôi cũng có kinh nghiệm trong quản lý dự án cũng như phân công công việc cho các thành viên trong nhóm để hoàn thành các dự án của công ty.",
            experience_itro3: "Ở giai đoạn này, tôi đã nghiên cứu các công nghệ phát triển web như Javascript, AngularJs, PHP, CodeIgniter MVC ...",
            experience_itro4: "Ở trường đại học, tôi đã học được kiến ​​thức sâu hơn về lập trình về thuật toán, cấu trúc dữ liệu,quy trình phát triển phần mềm..."
        },
        project_page: {
            title: "Dự Án ",
            label_filter: "Lọc",
            label_all: "Tất cả"

        },
        hobbies: {
            title: "Sở thích của tôi",
            reading: {
                title: "Học hỏi những điều mới trong sách",
                sub_title: "Tôi yêu thích việc đọc sách. Việc đọc sách có thể là một thói quen tuyệt nhất mà tôi có được. Nhờ việc đọc sách hằng ngày đã giúp tôi có thể bổ sung kiến thức và học hỏi rất nhiều từ sách"
            },
            workout: {
                title: "Tập Thể Dục",
                sub_title: "Tôi yêu thích việc tập gym vì nó giúp tôi tăng cường sức khỏe , giúp tôi có thể thư giãn cũng như giúp tôi có thể tái tạo năng lượng sau một ngày làm việc chăm chỉ."
            }
        },
        contact: {
            title: "Liên lạc với tôi",
            content: "Là một trong những châm ngôn trong cuộc sống mà tôi tâm đắc nhất. <br/>  " +
                "Tôi hy vọng với kĩ năng, kinh nghiệm của bản thân có thể giúp cho  team của bạn có thể phát triển và chuyên nghiệp hơn.",
            labels: {
                address: "Địa chỉ",
                call: "Liên hệ",
            }
        }
    }
}
