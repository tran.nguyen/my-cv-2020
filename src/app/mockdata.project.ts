export const majorProjects = [
    {
        name: "Intelisys Booking",
        type: "reactjs",
        img: "./assets/images/projects/itelisy-booking.jpeg",
        content: " <h2 style=\"text-align: center;\">Intelisys Booking Web App</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng:  <a href=\"https://www.intelisysaviation.com/\" target=\"_blank\" rel=\"noopener\"><strong>Intelisys</strong></a></li>\n" +
            "            <li>Mô tả dự án<br />Link: <a href=\"http://intelisys-web.dev-altamedia.com/\" target=\"_blank\">Project</a><br />Đây là dự án <strong>Intelisys</strong> tạo ra hệ thống Công cụ cho khách hàng đặt ghế chuyến bay và checkin online, quản lý các chuyến bay đã đặt trên nền tảng website.\n" +
            "                <img src=\"./assets/images/projects/flightbooking/home.png\" alt=\"\">\n" +
            "\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br />Frontend dự án được phát triển bằng<strong> Reactjs phiên bản 16.10.2</strong> (Reactjs Hook) ngoài ra:\n" +
            "                <ul>\n" +
            "                    <li><strong><em>Module builder Ant Design</em></strong> phát triển nhanh component của dự án</li>\n" +
            "                    <li><em><strong>Typescript: </strong></em></li>\n" +
            "                    <li>CSS tool: <em><strong>SASS</strong></em></li>\n" +
            "                    <li><em><strong>Rxjs: </strong></em>Axios Observable hỗ trợ cancel request thông qua unsubscribe ...</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong>Lead Project</strong><br />Công việc chính:\n" +
            "                <ul>\n" +
            "                    <li>Nhận yêu cầu từ BA và Leader để phân tích lên core dự án và luồng dữ liệu.</li>\n" +
            "                    <li>Phân chia task cụ thể cho team member cho dự án.</li>\n" +
            "                    <li>Support chuyên môn và hướng dẫn từng thành viên trong team để hoàn thành task đúng deadline.</li>\n" +
            "                    <li>Làm việc với bộ phận khác (Thiết kế, BA, Tester...) để lấy yêu cầu và giải quyết các thắc mắc của các thành viên trong team về dự án.</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Mockup dự án:<br /><a  href=\"https://drive.google.com/open?id=1fAaSDVGCgF3R_se93Op-7nNmyNBITHEy\" target=\"_blank\">Link mockup</a></li>\n" +
            "        </ol>"
    },
    {
        name: "Dam Sen 3D water screen",
        type: "angular",
        img: "./assets/images/projects/dam-sen.jpg",
        content: " <h2 style=\"text-align: center;\">Dam Sen 3D water screen</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: <a href=\"http://damsenpark.vn/\" target=\"_blank\" rel=\"noopener\"><strong>Đầm Sen Park</strong></a>\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\"> Đây là dự án quản lý trạng thái đóng mở hay ngắt kết nối của các thiết bị máy chiếu, tụ điện, và\n" +
            "                    điểu khiển đóng mở nguồn thiết bị.\n" +
            "                </p>\n" +
            "                <p>Hệ thống điều khiển phát và play video trên màn hình nước trên nền tảng website.</p>\n" +
            "                <img src=\"./assets/images/projects/man-hinh-nuoc.jpg\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng<strong> <strong>Angular 7</strong> (Backend: Nodejs, Unity: C#), </strong>, Công nghệ hỗ trợ:\n" +
            "                <ul>\n" +
            "                    <li><strong><strong>Socket IO</strong></strong> Kết nối backend quản lý realtime trạng thái của các thiết bị và điều khiển play video.</li>\n" +
            "                    <li><em><strong>Typescript ,CSS tool: <em><strong>SASS</strong></em></strong></em></li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong>Lead Project Frontend </strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Trao đổi và làm việc trực tiếp với Tech Lead dự án để nhận yêu cầu.</li>\n" +
            "                    <li>Làm việc trực tiếp với backend để thực hiện giao tiếp và tương tác API cho hệ thống.</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Bài viết dự án: <a href=\"http://damsenpark.vn/man-hinh-nuoc-singapore-se-co-tai-dam-sen-vao-27-4-2019/\"\n" +
            "                                     target=\"_blank\"> Bài Viết</a></li>\n" +
            "        </ol>"
    },
    {
        name: "Unilever CDE",
        type: "reactjs",
        img: "./assets/images/projects/unilever.png",
        content: "<h2 style=\"text-align: center;\">Unilever CDExcellent</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: <a href=\"https://www.unilever.com//\" target=\"_blank\" rel=\"noopener\"><strong>Unilever</strong></a>\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\">- CDE là các ứng dụng được chạy trên nền tảng máy tính để bàn và thiết bị di động (bao\n" +
            "                    gồm Android\n" +
            "                    và iOS) để đơn giản hóa và tăng cường huấn luyện năng suất thông qua Ứng dụng kỹ thuật số cho\n" +
            "                    Quản lý bán hàng.\n" +
            "                </p>\n" +
            "                <p>\n" +
            "                   - Đặt lịch viến thăm, tạo task, quản lý lịch viến thăm, quản lý khảo sát online,...\n" +
            "                </p>\n" +
            "                <img src=\"./assets/images/projects/unilever/CDExcellent.png\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng <strong> <strong>ReactJs</strong> (phiên bản\n" +
            "                16.10.2 Hook) (Backend: .Net). </strong> Công nghệ hỗ trợ:\n" +
            "                <ul>\n" +
            "                    <li>Module builder:<strong><strong> Ant Design</strong></strong> hỗ trợ phát triển nhanh các\n" +
            "                        component cho dự án\n" +
            "                    </li>\n" +
            "                    <li><em><strong>Typescript,Webpack ,CSS tool: <em><strong>SASS</strong></em></strong></em>...</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong> Dev</strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Lên core dự án, cài đặt môi trường</li>\n" +
            "                    <li>Nhận task từ leader , làm việc với backend để hoàn thành task được giao</li>\n" +
            "                    <li>Hỗ trợ debug và kĩ thuật cho team</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Một Số module phần mềm:\n" +
            "                <ul>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/calendar.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Tạo lịch viến thăm</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/history.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí lịch viến thăm</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/u-user.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí người dùng</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/u-area.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí Vùng</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/u-media.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Upload , quản lí media</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/u-article.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí bài viết</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/unilever/u-survey.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí Khảo sát</p></li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "        </ol>"
    },
    {
        name: "KPMG Clubhouse",
        type: "angular",
        img: "./assets/images/projects/club-house-kpmg.jpg",
        content: " <h2 style=\"text-align: center;\">KPMG Clubhouse</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: <a href=\"https://home.kpmg/sg/en/home.html\" target=\"_blank\" rel=\"noopener\"><strong>KPMG\n" +
            "                Singapore</strong></a>\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\">-Hệ thống detect Nhận diện khuôn mặt khách hàng ra vào Clubhouse.\n" +
            "                </p>\n" +
            "\n" +
            "                <img src=\"./assets/images/projects/kpmg/kpmg-thumnail.png\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng <strong> <strong>Angualar 5</strong> (Backend:\n" +
            "                PHP -Framework: laravel). </strong> Công nghệ hỗ trợ:\n" +
            "                <ul>\n" +
            "                    <li>SocketIO hỗ trợ điều khiển hiển thị bật tắt trình chiếu màn hình chào</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong> Lead Frontend</strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Xây dựng , cài đặt môi trường cho dự án</li>\n" +
            "                    <li>Làm việc với backend nhận yêu cầu vào tương tác API</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Một Số module phần mềm:\n" +
            "                <ul>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-screen.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Màn hình chào</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-home.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Thống kê vùng và số lượng khách hàng có trong clubhouse</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-home-sidebar.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Thống kê người theo từng nhóm</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-show-face.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Nhận diện khuôn mặt</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-home-detail.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Thông tin căn bản, lịch sử của khách hàng</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-chart.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Thống kê theo ngày</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-chart-month.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Thống kê theo tháng</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-staff.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí khách hàng</p>\n" +
            "                    </li>\n" +
            "                    <li><img src=\"./assets/images/projects/kpmg/kpmg-fred.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí lịch hiện slide trên màn hình chào</p>\n" +
            "                    </li>\n" +
            "\n" +
            "\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "        </ol>"
    },
    {
        name: "Duty Free Warehouse",
        type: "reactjs",
        img: "./assets/images/projects/ware-house.jpeg",
        content: " <h2 style=\"text-align: center;\">Duty Free Warehouse</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: <a href=\"http://www.vietjetair.com/\" target=\"_blank\" rel=\"noopener\"><strong>VietJet Air</strong></a>\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\">- Duty Free Warehouse là phần mềm quản lí kho cho các chuyến bay của  Vietjet. Bao gồm:\n" +
            "                </p>\n" +
            "                <ol class=\"ml-3\">\n" +
            "                    <li>Quản lí Tờ Khai online</li>\n" +
            "                    <li>Quản lí điều chuyển</li>\n" +
            "                    <li>Báo cáo xuất nhập kho</li>\n" +
            "                    <li>Quản lí sản phẩm</li>\n" +
            "                    <li>Quản lí chặng bay</li>\n" +
            "                    <li>Quản lí người dùng</li>\n" +
            "                </ol>\n" +
            "                <img src=\"./assets/images/projects/ware-house.jpeg\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng <strong> <strong>ReactJs</strong> (phiên bản\n" +
            "                16.10.2 Hook) (Backend: .Net). </strong> Công nghệ hỗ trợ:\n" +
            "                <ul>\n" +
            "                    <li>Module builder:<strong><strong> Ant Design</strong></strong> hỗ trợ phát triển nhanh các\n" +
            "                        component cho dự án\n" +
            "                    </li>\n" +
            "                    <li><em><strong>Typescript,Webpack ,CSS tool: <em><strong>SASS</strong></em></strong></em>...</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong> Dev</strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Lên core dự án, cài đặt môi trường</li>\n" +
            "                    <li>Nhận task từ leader , làm việc với backend để hoàn thành task được giao</li>\n" +
            "                    <li>Hỗ trợ debug và kĩ thuật cho team</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Một Số module phần mềm:\n" +
            "                <ul>\n" +
            "                    <li><img src=\"./assets/images/projects/vj/df-tokhai.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí tờ khai</p></li>\n" +
            "                 <li><img src=\"./assets/images/projects/vj/df-qldc.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí điều chuyển</p></li>\n" +
            "                 <li><img src=\"./assets/images/projects/vj/df-baocao.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí báo cáo</p></li>\n" +
            "                 <li><img src=\"./assets/images/projects/vj/df-qlsp.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí sản phẩm</p></li>\n" +
            "                 <li><img src=\"./assets/images/projects/vj/df-qlcb.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí chặn bay</p></li>\n" +
            "                 <li><img src=\"./assets/images/projects/vj/df-qlnd.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Quản lí người dùng</p></li>\n" +
            "\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "        </ol>"
    },
    {
        name: "HydroElectricity Camera",
        type: "angular",
        img: "./assets/images/projects/hydro-electricity.jpg",
        content: "<h2 style=\"text-align: center;\">HydroElectricity Camera</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: Thủy điện Ankroet Đà Lạt\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\">-Phầm mềm quản lí các camera của hệ thống thủy điện, cài đặt và quản lí các cảm ứng nguy hiểm\n" +
            "                    kết hợp với hệ thống camera để bảo vệ an toàn cho thủy điện.\n" +
            "                </p>\n" +
            "\n" +
            "                <img src=\"./assets/images/projects/hydro-electricity.jpg\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng <strong> <strong>ReactJS (phiên bản 16.8 React\n" +
            "                Hook)</strong> (Backend: .NET). </strong>\n" +
            "                Kỹ thuật chính đó là đăng nhập và service của camera để có thể stream được video từ camera đồng thời gửi\n" +
            "                các tín hiệu điều khiển quay các con camera.\n" +
            "                <p>Sử dụng công nghệ <strong>SocketIO</strong> để stream video và bắt tín hiệu realtime từ các thiết bị cảnh báo</p>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong> Lead Frontend</strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Làm việc trực tiếp với backend để thực hiện các yêu cầu chức năng từ khách hàng.</li>\n" +
            "                    <li>Tìm hiểu phương thức và cách thức để đăng nhập vào con camera để điều khiển được camera.</li>\n" +
            "                    <li>Làm việc với backend để quản lí hệ thống nhiều camera và nhiều cảm biến nguy hiểm.</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Một Số module chính trong project:\n" +
            "                <ul>\n" +
            "                    <li><img src=\"./assets/images/projects/hydro-electricity/hydroElectric-dashboard.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Sơ đồ quản lí vị trí thiết bị</p></li>\n" +
            "\n" +
            "                    <li><img src=\"./assets/images/projects/hydro-electricity/hydroElectric-control-stream.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Điều khiển và stream video</p></li>\n" +
            "\n" +
            "\n" +
            "\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "        </ol>"
    },
    {
        name: "Intelisys CMS",
        type: "reactjs",
        img: "./assets/images/projects/perimeter-cms.jpeg",
        content: "<h2 style=\"text-align: center;\">Intelisys CMS</h2>\n" +
            "        <ol style=\"list-style-type: upper-roman;\">\n" +
            "            <li>Khách Hàng: <a href=\"https://www.intelisysaviation.com\" target=\"_blank\"\n" +
            "                               rel=\"noopener\"><strong>Intelisys</strong></a>\n" +
            "            </li>\n" +
            "            <li>Mô tả dự án (Dự án nội bộ)\n" +
            "                <p class=\"mb-0\">-CMS quản lí bài viết, Xây dựng content cho bên App booking mobile và App booking Web sử\n" +
            "                    dụng.\n" +
            "                </p>\n" +
            "\n" +
            "                <img src=\"./assets/images/projects/perimeter-cms.jpeg\" alt=\"\">\n" +
            "            </li>\n" +
            "\n" +
            "            <li>Công nghệ sử dụng<br/>Frontend dự án được phát triển bằng <strong> <strong>ReactJS (phiên bản 16.8 React\n" +
            "                Hook)</strong> (Backend: .NET). </strong>\n" +
            "            </li>\n" +
            "            <li>Vai trò trong dự án: <strong> Lead Frontend</strong><br/>\n" +
            "                <ul>\n" +
            "                    <li>Nhận yêu cầu từ BA, leader.</li>\n" +
            "                    <li>Cài đặt môi trường, và lên khung giao diện cho nền tảng web</li>\n" +
            "                    <li>Làm việc với backend để thực hiện đổ dữ liệu cho dự án</li>\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "            <li>Một Số module chính trong project:\n" +
            "                <ul>\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-dashboard.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Tổng quan tất cả Category và Article</p></li>\n" +
            "\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-category.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Module quản lí Category</p></li>\n" +
            "\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-articles.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Module quản lí Article</p></li>\n" +
            "\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-notification.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Module quản lí Notification</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-parameter.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Module quản lí Parameter</p></li>\n" +
            "                    <li><img src=\"./assets/images/projects/intelisys-cms/intelisys-cms-permision.png\" alt=\"\">\n" +
            "                        <p class=\"text-center text-info\">Module quản lí Quyền</p></li>\n" +
            "\n" +
            "\n" +
            "                </ul>\n" +
            "            </li>\n" +
            "        </ol>"
    },

]
export const initArticle = {
    name: "Intelisys Booking",
    content: "<div id=\"primary\" class=\"style-scope ytd-watch-flexy\">\n" +
        "<div id=\"primary-inner\" class=\"style-scope ytd-watch-flexy\">\n" +
        "<div id=\"player\" class=\"style-scope ytd-watch-flexy\">\n" +
        "<div id=\"player-container-outer\" class=\"style-scope ytd-watch-flexy\">\n" +
        "<div id=\"player-container-inner\" class=\"style-scope ytd-watch-flexy\">\n" +
        "<div id=\"player-container\" class=\"style-scope ytd-watch-flexy\" role=\"complementary\">\n" +
        "<div id=\"container\" class=\"style-scope ytd-player\">\n" +
        "<div id=\"movie_player\" class=\"html5-video-player ytp-transparent ytp-exp-contextmenu-icons ytp-hide-info-bar ytp-large-width-mode ytp-iv-drawer-enabled playing-mode ytp-autohide\" tabindex=\"-1\" data-version=\"/yts/jsbin/player_ias-vflHB5qdo/en_US/base.js\" aria-label=\"YouTube Video Player\">\n" +
        "<div class=\"ytp-player-content ytp-iv-player-content\" data-layer=\"4\">\n" +
        "<div class=\"annotation annotation-type-custom iv-branding\">\n" +
        "<div class=\"branding-img-container\">\n" +
        "<div class=\"uk-container uk-container-center\">\n" +
        "<div id=\"benefits-wrapper\" class=\"uk-grid\">\n" +
        "<div class=\"uk-width-1-1 uk-width-medium-3-5 uk-width-large-1-2 uk-container-center\">\n" +
        "<h2 id=\"benefits\" class=\"uk-text-center h2 h--slim color--secondary\">A flexible design solution that adapts to your workflow.</h2>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"uk-grid uk-flex-middle summary__item\" data-uk-grid-margin=\"\">\n" +
        "<div class=\"uk-width-small-1-1 uk-width-medium-1-2 uk-flex-middle\">\n" +
        "<div class=\"summary__item__content summary__item__content--left\">\n" +
        "<h3 class=\"h3 color--secondary\">Work Anywhere With Your Team</h3>\n" +
        "<p class=\"color__text--secondary\">Work remotely in the Cloud - anytime and on any device - without the hassle of uploading and downloading files.</p>\n" +
        "<p class=\"color__text--secondary\">Keep a geographically dispersed team collaborating - and always in sync.</p>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"uk-width-small-1-1 uk-width-medium-1-2 uk-hidden-small summary__item__picture--right\"><img class=\" ls-is-cached lazyloaded\" src=\"https://landing.moqups.com/img/illustrations/illu1@2x.png\" alt=\"Cloud synchronisation\" data-src=\"https://landing.moqups.com/img/illustrations/illu1@2x.png\" data-rendered=\"true\" /></div>\n" +
        "</div>\n" +
        "<div class=\"uk-grid uk-flex-middle summary__item\">\n" +
        "<div class=\"uk-width-small-1-1 uk-width-medium-1-2\">\n" +
        "<div class=\"uk-width-medium-1-1 uk-container-center summary__item__picture summary__item__picture--left\"><img class=\" lazyloaded\" src=\"https://landing.moqups.com/img/illustrations/illu2@2x.png\" alt=\"Fast learning curve\" data-src=\"https://landing.moqups.com/img/illustrations/illu2@2x.png\" data-rendered=\"true\" /></div>\n" +
        "</div>\n" +
        "<div class=\"uk-width-small-1-1 uk-width-medium-1-2 uk-flex-middle\">\n" +
        "<div class=\"summary__item__content summary__item__content--right\">\n" +
        "<h3 class=\"h3 color--secondary\">One Fast Learning Curve For Everyone</h3>\n" +
        "<p class=\"color__text--secondary\">Get collaborators onboard quickly with a single, intuitive interface.</p>\n" +
        "<p class=\"color__text--secondary\">Curated design tools provide the functionality and flexibility that your team really needs - without any unnecessary or distracting complexity.</p>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"uk-grid uk-flex-middle summary__item\">\n" +
        "<div class=\"uk-width-small-1-1 uk-width-medium-1-2 uk-flex-middle\">\n" +
        "<div class=\"summary__item__content summary__item__content--left\">\n" +
        "<h3 class=\"h3 color--secondary\">Accelerate Your Creative Process</h3>\n" +
        "<p class=\"color__text--secondary\">Work within a single creative context to maintain your team's focus and momentum.</p>\n" +
        "<p class=\"color__text--secondary\">Keep all stakeholders &ndash; Product Managers, Business Analysts, System Architects, Designers and Developers &ndash; building consensus and communicating clearly.</p>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"uk-width-medium-1-2 uk-hidden-small summary__item__picture--right\"><img class=\" lazyloaded\" src=\"https://landing.moqups.com/img/illustrations/illu3@2x.png\" alt=\"Unlimited users subscription\" data-src=\"https://landing.moqups.com/img/illustrations/illu3@2x.png\" data-rendered=\"true\" /></div>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"section__wrapper sides__section__wrapper\">\n" +
        "<div class=\"section\">\n" +
        "<div id=\"features-wrapper\">\n" +
        "<div class=\"uk-grid hero__sides\">\n" +
        "<div class=\"uk-width-medium-1-1 uk-width-large-1-3\"><img class=\"uk-float-left hero__side hero__side--left lazyloaded\" src=\"https://landing.moqups.com/img/frames/confetti-right@2x.png\" alt=\"Confetti right\" data-src=\"https://landing.moqups.com/img/frames/confetti-right@2x.png\" data-rendered=\"true\" /></div>\n" +
        "<div class=\"uk-width-medium-1-1 uk-width-large-1-3 uk-vertical-align uk-text-center\">&nbsp;\n" +
        "<div class=\"hero__side__content uk-vertical-align-middle\">\n" +
        "<h2 id=\"features\" class=\"h2 color--secondary hero__side__title\">A full ecosystem of tools<br />within a single design environment.</h2>\n" +
        "<div class=\"uk-margin-large-bottom\">\n" +
        "<p class=\"color__text--secondary\">Go from diagrams, wireframes and prototypes without switching apps or updating across platforms.</p>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "<div class=\"uk-width-medium-1-1 uk-width-large-1-3\"><img class=\"uk-float-right hero__side hero__side--right lazyloaded\" src=\"https://landing.moqups.com/img/frames/confetti-left@2x.png\" alt=\"Confetti left\" data-src=\"https://landing.moqups.com/img/frames/confetti-left@2x.png\" data-rendered=\"true\" /></div>\n" +
        "</div>\n" +
        "<div class=\"showcase__header\">\n" +
        "<div class=\"uk-hidden-small showcase--large\"><img class=\" lazyloaded\" src=\"https://landing.moqups.com/img/product-shots/moqups-overview.png\" alt=\"Moqups Overview\" data-src=\"https://landing.moqups.com/img/product-shots/moqups-overview.png\" data-rendered=\"true\" /></div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>"
};
