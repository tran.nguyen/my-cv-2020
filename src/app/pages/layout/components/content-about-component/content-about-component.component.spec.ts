import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentAboutComponentComponent } from './content-about-component.component';

describe('ContentAboutComponentComponent', () => {
  let component: ContentAboutComponentComponent;
  let fixture: ComponentFixture<ContentAboutComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentAboutComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentAboutComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
