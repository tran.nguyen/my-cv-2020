import { Component, OnInit } from '@angular/core';
import {LocalLanguage} from "../../../../locals/local.language";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../app.state";

@Component({
  selector: 'app-content-about-component',
  templateUrl: './content-about-component.component.html',
  styleUrls: ['./content-about-component.component.scss']
})
export class ContentAboutComponentComponent implements OnInit {
  localContent = LocalLanguage.vi.resume_page;
  language$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.language$ = store.select("language"); // lấy Language reducer từ store
  }

  ngOnInit() {
    this.language$.subscribe(language => {
      this.localContent = LocalLanguage[language].resume_page;
    });
  }

}
