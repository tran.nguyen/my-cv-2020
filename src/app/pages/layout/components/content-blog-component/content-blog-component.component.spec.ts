import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentBlogComponentComponent } from './content-blog-component.component';

describe('ContentBlogComponentComponent', () => {
  let component: ContentBlogComponentComponent;
  let fixture: ComponentFixture<ContentBlogComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentBlogComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentBlogComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
