import { Component, OnInit } from '@angular/core';
import {LocalLanguage} from "../../../../locals/local.language";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../app.state";

@Component({
  selector: 'app-content-blog-component',
  templateUrl: './content-blog-component.component.html',
  styleUrls: ['./content-blog-component.component.scss']
})
export class ContentBlogComponentComponent implements OnInit {
  localContent = LocalLanguage.vi.hobbies;
  language$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.language$ = store.select("language"); // lấy Language reducer từ store
  }

  ngOnInit() {
    this.language$.subscribe(language => {
      this.localContent = LocalLanguage[language].hobbies;
    });
  }
}
