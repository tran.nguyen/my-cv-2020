import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentContactComponentComponent } from './content-contact-component.component';

describe('ContentContactComponentComponent', () => {
  let component: ContentContactComponentComponent;
  let fixture: ComponentFixture<ContentContactComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentContactComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentContactComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
