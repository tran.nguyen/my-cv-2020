import { Component, OnInit } from '@angular/core';
import {LocalLanguage} from "../../../../locals/local.language";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../app.state";

@Component({
  selector: 'app-content-contact-component',
  templateUrl: './content-contact-component.component.html',
  styleUrls: ['./content-contact-component.component.scss']
})
export class ContentContactComponentComponent implements OnInit {

  localContent = LocalLanguage.vi.contact;
  language$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.language$ = store.select("language"); // lấy Language reducer từ store
  }

  ngOnInit() {
    this.language$.subscribe(language => {
      this.localContent = LocalLanguage[language].contact;
    });
  }

}
