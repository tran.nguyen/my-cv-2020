import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentPortfolioComponentComponent } from './content-portfolio-component.component';

describe('ContentPortfolioComponentComponent', () => {
  let component: ContentPortfolioComponentComponent;
  let fixture: ComponentFixture<ContentPortfolioComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentPortfolioComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentPortfolioComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
