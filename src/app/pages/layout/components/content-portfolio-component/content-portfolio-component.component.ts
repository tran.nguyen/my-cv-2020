import {Component, OnInit} from '@angular/core';
import {majorProjects} from "../../../../mockdata.project";
import {Store, select} from "@ngrx/store";
import {AppState} from "../../../../app.state";
import * as ArticleActions from "@stores/actions/article.action";
import {LocalLanguage} from "../../../../locals/local.language";
import {Observable} from "rxjs";
@Component({
    selector: 'app-content-portfolio-component',
    templateUrl: './content-portfolio-component.component.html',
    styleUrls: ['./content-portfolio-component.component.scss']
})
export class ContentPortfolioComponentComponent implements OnInit {
    public majorProjectData = majorProjects;
    localContent = LocalLanguage.vi.project_page;
    language$: Observable<any>;

    constructor(private store: Store<AppState>) {
        this.language$ = store.select("language"); // lấy Language reducer từ store
    }

    ngOnInit() {
        this.language$.subscribe(language => {
            this.localContent = LocalLanguage[language].project_page;
        });
    }

    setArticle(article) {
        this.store.dispatch(new ArticleActions.SetArticleAction(article));
    }


}
