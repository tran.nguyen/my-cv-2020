import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBlockComponentComponent } from './menu-block-component.component';

describe('MenuBlockComponentComponent', () => {
  let component: MenuBlockComponentComponent;
  let fixture: ComponentFixture<MenuBlockComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBlockComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBlockComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
