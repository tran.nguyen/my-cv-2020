import { Component, OnInit } from '@angular/core';
import {LocalLanguage} from "../../../../locals/local.language";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../app.state";
import {Observable} from "rxjs";

@Component({
  selector: 'app-menu-block-component',
  templateUrl: './menu-block-component.component.html',
  styleUrls: ['./menu-block-component.component.scss']
})
export class MenuBlockComponentComponent implements OnInit {
  localContent = LocalLanguage.vi.menu;
  language$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.language$ = store.select("language"); // lấy Language reducer từ store
  }

  ngOnInit() {
    console.log(LocalLanguage)
    this.language$.subscribe(language => {
      this.localContent = LocalLanguage[language].menu;
    });
  }

}
