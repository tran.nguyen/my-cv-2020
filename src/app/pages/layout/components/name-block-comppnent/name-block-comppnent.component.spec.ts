import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameBlockComppnentComponent } from './name-block-comppnent.component';

describe('NameBlockComppnentComponent', () => {
  let component: NameBlockComppnentComponent;
  let fixture: ComponentFixture<NameBlockComppnentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameBlockComppnentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameBlockComppnentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
