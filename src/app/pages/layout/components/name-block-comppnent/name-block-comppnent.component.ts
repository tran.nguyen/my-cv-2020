import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../app.state";
import {SetLanguageAction} from "@stores/actions/language.action";
import {LocalLanguage} from "../../../../locals/local.language";

@Component({
    selector: 'app-name-block-comppnent',
    templateUrl: './name-block-comppnent.component.html',
    styleUrls: ['./name-block-comppnent.component.scss']
})
export class NameBlockComppnentComponent implements OnInit {
    localContent = LocalLanguage.vi.name_page;
    language = "";
    language$: Observable<any>;

    constructor(private store: Store<AppState>) {
        this.language$ = store.select("language"); // lấy Language reducer từ store
    }

    ngOnInit() {
        this.language$.subscribe(language => {
            this.language = language;
            this.localContent = LocalLanguage[language].name_page;
        });
    }

    changeLanguage(lg) {
        this.store.dispatch(new SetLanguageAction(lg));

    }

}
