import {Component, OnInit} from '@angular/core';
import {majorProjects} from "../../../../mockdata.project";
import {Observable} from "rxjs";
import {Store, select} from "@ngrx/store";
import {AppState} from "../../../../app.state";

@Component({
    selector: 'app-show-article',
    templateUrl: './show-article.component.html',
    styleUrls: ['./show-article.component.scss']
})
export class ShowArticleComponent implements OnInit {
    public showArticle: { name: string, content } = {name: "", content: ""};
    article$: Observable<any>;
    language$: Observable<any>;

    constructor(private store: Store<AppState>) {
        this.article$ = store.select("article"); // lấy article reducer từ store
        this.language$ = store.select("language"); // lấy article reducer từ store
    }

    ngOnInit() {
        this.article$.subscribe(article => {
            this.showArticle = article;
        });
        this.language$.subscribe(language => {
            console.log(language);
        });
    }

}
