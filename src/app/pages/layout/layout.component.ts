import {Component, OnInit} from '@angular/core';
import {runJS} from "../../../functions";
import {LocalLanguage} from "../../locals/local.language";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../app.state";


@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    localContent = LocalLanguage.vi.menu;
    language$: Observable<any>;

    constructor(private store: Store<AppState>) {
        this.language$ = store.select("language"); // lấy Language reducer từ store
    }

    ngOnInit() {
        runJS();
        this.language$.subscribe(language => {
            this.localContent = LocalLanguage[language].menu;
        });
    }



}
