import {NgModule} from '@angular/core';
import {LayoutRoutingModule} from './layout-routing.module';
import {LayoutComponent} from './layout.component';
import {CalendarModule} from 'primeng/calendar';
import {FormsModule} from "@angular/forms";
import { NameBlockComppnentComponent } from './components/name-block-comppnent/name-block-comppnent.component';
import { MenuBlockComponentComponent } from './components/menu-block-component/menu-block-component.component';
import { ContentAboutComponentComponent } from './components/content-about-component/content-about-component.component';
import { ContentPortfolioComponentComponent } from './components/content-portfolio-component/content-portfolio-component.component';
import { ContentBlogComponentComponent } from './components/content-blog-component/content-blog-component.component';
import { ContentContactComponentComponent } from './components/content-contact-component/content-contact-component.component';
import { ShowArticleComponent } from './components/show-article/show-article.component';
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [LayoutComponent, NameBlockComppnentComponent, MenuBlockComponentComponent, ContentAboutComponentComponent, ContentPortfolioComponentComponent, ContentBlogComponentComponent, ContentContactComponentComponent, ShowArticleComponent],
    imports: [
        FormsModule,
        CommonModule,
        CalendarModule,
        LayoutRoutingModule,
    ]
})
export class LayoutModule {
}
