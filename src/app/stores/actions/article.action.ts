import {Injectable} from "@angular/core";
import {Action} from '@ngrx/store';


export const SET_ARTICLE = '[showArticle Component] setArticle';

export class SetArticleAction implements Action {
    readonly type = SET_ARTICLE;
    constructor(public payload: any) {
    }

}

export type Actions = SetArticleAction;
