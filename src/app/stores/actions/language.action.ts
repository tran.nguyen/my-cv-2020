import {Action} from '@ngrx/store';


export const SET_LANGUAGE = '[Language] setLanguage';

export class SetLanguageAction implements Action {
    readonly type = SET_LANGUAGE;
    constructor(public payload: string) {
    }

}

export type Actions = SetLanguageAction;
