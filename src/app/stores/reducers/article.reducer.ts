import {initArticle} from "../../mockdata.project";

import * as ArticleActions from "./../actions/article.action";

export function ArticleReducer(state = initArticle, action: ArticleActions.Actions) {
    switch (action.type) {
        case ArticleActions.SET_ARTICLE: {
            console.log(action.payload);
            return {...action.payload};
        }
        default: {
            console.log("No action here");
            return state;
        }
    }
}

