import * as LanguageActions from "./../actions/language.action";

export function LanguageReducer(state = "vi", action: LanguageActions.Actions) {
    switch (action.type) {
        case LanguageActions.SET_LANGUAGE: {
            return action.payload;
        }
        default: {
            return state;
        }
    }
}

